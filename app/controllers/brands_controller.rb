class BrandsController < ApplicationController
	def index
		@brands = Brand.all
	end

	def show
		@brand = Brand.find(params[:id])
		@products = @brand.items.sort
    	productsPerLine = 4
    	temp = []
	    @productGroups = []
	    count = productsPerLine
	    @products.each do |product|
			product.refresh
			temp << product
			count -= 1
			if count == 0 
				count = productsPerLine
				@productGroups << temp
				temp = []
			end
	    end
	    if temp.count > 0
	     	@productGroups << temp
	    end
	end
end
