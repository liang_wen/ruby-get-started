class UsersController < ApplicationController
	before_action :authenticate_user!, :check_authorization
	def index
		@users = User.all
		# @buyers = User.where("role = ?", 0)
	end

  	def edit
  		@user = User.find(params[:id])
  	end

  	def update
  		@user = User.find(params[:id])
  		if params[:user][:role].nil?
  			nil
  		else
  			if params[:user][:role] == "admin"
  				raise User::NotAuthorized
  			else
  				@user.role = params[:user][:role]
  			end
  		end
  		@user.distribution = params[:user][:distribution]
  		@user.name = params[:user][:name]
  		respond_to do |format|
			if @user.save
				format.html { redirect_to users_path, notice: 'User info was successfully updated.' }
				# format.json { render :show, status: :created, location: @parcel }
			else
				format.html { render :new }
				# format.json { render json: @parcel.errors, status: :unprocessable_entity }
			end
	    end
  	end

  	private
	    # If the user is not authorized, just throw the exception.
	    def check_authorization
	      raise User::NotAuthorized unless current_user.admin?
	    end
end