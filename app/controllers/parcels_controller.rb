class ParcelsController < ApplicationController
	before_action :authenticate_user!
	def index
	end

	def new
		@buyers = User.where("role = ?", 0)
		@parcel = Parcel.new
	end

	def create
		@seller = current_user
		@buyer = User.find(params[:parcel][:buyer_id])
		date = Date.new(params[:parcel]['shiped_date(1i)'].to_i,params[:parcel]['shiped_date(2i)'].to_i,params[:parcel]['shiped_date(3i)'].to_i)
	    @parcel = Parcel.new
	    @parcel.buyer = @buyer
	    @parcel.seller = @seller
	    @parcel.track_no = params[:parcel][:track_no]
	    @parcel.shiped_date = date
	    @parcel.delivered = params[:parcel][:delivered]
	    @parcel.description = params[:parcel]['description']
	    respond_to do |format|
			if @parcel.save
				format.html { redirect_to parcels_path, notice: 'Parcel was successfully created.' }
				# format.json { render :show, status: :created, location: @parcel }
			else
				format.html { render :new }
				# format.json { render json: @parcel.errors, status: :unprocessable_entity }
			end
	    end
  	end

  	def edit
  		@buyers = User.where("role = ?", 0)
  		@parcel = Parcel.find(params[:id])
  	end

  	def update
  		@parcel = Parcel.find(params[:id])
  		@buyer = User.find(params[:parcel][:buyer_id])
  		date = Date.new(params[:parcel]['shiped_date(1i)'].to_i,params[:parcel]['shiped_date(2i)'].to_i,params[:parcel]['shiped_date(3i)'].to_i)
	    @parcel.buyer = @buyer
	    @parcel.track_no = params[:parcel][:track_no]
	    @parcel.shiped_date = date
	    @parcel.delivered = params[:parcel][:delivered]
	    @parcel.description = params[:parcel]['description']
  		respond_to do |format|
			if @parcel.save
				format.html { redirect_to parcels_path, notice: 'Parcel was successfully updated.' }
				# format.json { render :show, status: :created, location: @parcel }
			else
				format.html { render :new }
				# format.json { render json: @parcel.errors, status: :unprocessable_entity }
			end
	    end
  	end
end