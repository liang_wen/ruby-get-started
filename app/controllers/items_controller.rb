class ItemsController < ApplicationController
	def bestSellers
		@items = Item.where("best_seller = ?", true)
		itemsPerLine = 4
	    temp = []
	    @itemGroups = []
	    count = itemsPerLine
	    @items.each do |item|
	      item.refresh
	      temp << item
	      count -= 1
	      if count == 0 
	      	count = itemsPerLine
	      	@itemGroups << temp
	      	temp = []
	      end
	    end
	    if temp.count > 0
	      @itemGroups << temp
	    end
	end

	def discount
		@allItems = Item.all
		@allItems.each{|item|item.refresh}
		@items = Item.where("special = ?", true).order(:brand_id, :identifier)
		itemsPerLine = 4
	    temp = []
	    @itemGroups = []
	    count = itemsPerLine
	    @items.each do |item|
	      temp << item
	      count -= 1
	      if count == 0 
	      	count = itemsPerLine
	      	@itemGroups << temp
	      	temp = []
	      end
	    end
	    if temp.count > 0
	      @itemGroups << temp
	    end
	end
end