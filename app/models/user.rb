class User < ActiveRecord::Base
	has_many :inParcels, foreign_key: "buyer_id", class_name: "Parcel"
	has_many :outParcels, foreign_key: "seller_id", class_name: "Parcel"
	enum role: [:buyer, :seller, :admin]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

    def name_with_email
  		self.name + ", " +self.email
  	end
end