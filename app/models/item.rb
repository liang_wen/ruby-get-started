require 'nokogiri'
require 'open-uri'
BASE_URL = "https://www.chemistwarehouse.com.au/"
PREFIX = "search?searchtext="
SUFFIX = "&searchmode=allwords"

class Item < ActiveRecord::Base
	belongs_to :brand

	def refresh()
        if self.lastUpdatedDate != Date.today
      		doc = Nokogiri::HTML(open("#{BASE_URL}#{PREFIX}#{self.identifier}#{SUFFIX}"))

            href = nil
            img = nil
            price = nil
            name = nil
            inStock = nil

            doc.css("//div.Product//a").each_with_index do |product, index|
                href = product.attr :href
                identifier = href.match(/\/Buy\/(\d*)\//)[1]
                # byebug
                if identifier.to_i == self.identifier
                    img = product.css("img").first.attr :src
                    price = product.css("span").text.gsub(/\s+/,"").match(/\$(\d*.\d*)(SAVE\$\d*.\d*)?/)[1]
                    name = product.attr :title
                    docDetail = Nokogiri::HTML(open("#{BASE_URL}#{href}"))
                    inStock = docDetail.css("//div.productDetail//div")[1].text.match(/Temporarily Low Stock Online - Please Try Your Local Store/).nil?
                    break
                end
                href = nil
            end
        	self.name = name
        	self.price = price.to_f
        	self.img = img
        	self.href = href
            self.inStock = inStock
            
            if self.higheast_price.nil?
                self.higheast_price = self.price
            else
                self.higheast_price = [self.higheast_price, self.price].max
            end

            if self.price < self.higheast_price
                self.special = true
            else
                self.special = false
            end

            self.lastUpdatedDate = Date.today
        	self.save
        end
	end
end
