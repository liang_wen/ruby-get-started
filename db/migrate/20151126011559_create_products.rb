class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :identifier
      t.text :name
      t.float :price
      t.text :img
      t.text :href
      t.date :lastUpdatedDate
      t.timestamps null: false
    end
  end
end
