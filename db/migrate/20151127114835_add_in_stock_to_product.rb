class AddInStockToProduct < ActiveRecord::Migration
  def change
    add_column :products, :inStock, :boolean
  end
end
