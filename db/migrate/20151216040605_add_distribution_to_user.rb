class AddDistributionToUser < ActiveRecord::Migration
  def change
  	add_column :users, :distribution, :boolean, default: false
  end
end