class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :identifier
      t.text :name
      t.float :price
      t.text :img
      t.text :href
      t.date :lastUpdatedDate
      t.string :description
      t.boolean :inStock
      t.timestamps null: false

      t.belongs_to :brand, index: true
    end
  end
end
