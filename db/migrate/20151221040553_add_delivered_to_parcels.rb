class AddDeliveredToParcels < ActiveRecord::Migration
  def change
  	add_column :parcels, :delivered, :boolean, default: false
  end
end