class AddRoleToUser < ActiveRecord::Migration
  def change
  	add_column :users, :role, :integer, default: "buyer"
  	add_column :users, :name, :string
  end
end
