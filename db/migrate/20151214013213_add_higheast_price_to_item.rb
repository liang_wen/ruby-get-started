class AddHigheastPriceToItem < ActiveRecord::Migration
  def change
    add_column :items, :higheast_price, :float
    add_column :items, :special, :boolean
    add_column :items, :best_seller, :boolean
  end
end