class CreateParcels < ActiveRecord::Migration
  def change
    create_table :parcels do |t|
      t.string :track_no
      t.date :shiped_date
      t.string :description

      t.timestamps null: false
      t.belongs_to :buyer, index: true
      t.belongs_to :seller, index: true
    end
  end
end
