# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151230060726) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.text     "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.integer  "identifier"
    t.text     "name"
    t.float    "price"
    t.text     "img"
    t.text     "href"
    t.date     "lastUpdatedDate"
    t.string   "description"
    t.boolean  "inStock"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "brand_id"
    t.float    "higheast_price"
    t.boolean  "special"
    t.boolean  "best_seller"
  end

  add_index "items", ["brand_id"], name: "index_items_on_brand_id", using: :btree

  create_table "parcels", force: :cascade do |t|
    t.string   "track_no"
    t.date     "shiped_date"
    t.string   "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "buyer_id"
    t.integer  "seller_id"
    t.boolean  "delivered",   default: false
  end

  add_index "parcels", ["buyer_id"], name: "index_parcels_on_buyer_id", using: :btree
  add_index "parcels", ["seller_id"], name: "index_parcels_on_seller_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "role",                   default: 0
    t.string   "name",                   default: ""
    t.boolean  "distribution",           default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
