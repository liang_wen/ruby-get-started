namespace :brand do
  task :init => :environment do
    names = [
      "Swisse",
      "Blackmores",
      "HealthyCare",
      "Nature'sWay",
      "BioIsland",
      "Nulax",
      "Lucas",
      "Other"
    ]

    names.each do |name|
      Brand.find_or_create_by(name: name)
    end
  end
end