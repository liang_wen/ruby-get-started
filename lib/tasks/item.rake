namespace :item do
  task :migrate => :environment do
    ids = [
      {"id"=>74931,"description"=>"Swisse 卵磷脂(1200mg) 150粒","brand"=>"Swisse"},
      {"id"=>70243,"description"=>"HealthyCare 卵磷脂(1200mg) 100粒","brand"=>"HealthyCare"}
    ]
    ids.each do |id|
      @brand = Brand.find_or_create_by(name: id["brand"])
      @item = Item.find_or_create_by(identifier: id["id"])
      @item.description = id["description"]
      @item.brand = @brand
      @item.refresh()
    end
  end

  task :updateAll => :environment do
    @items = Item.all
    @items.each do |item|
      item.lastUpdatedDate = Date.today - 1
      item.save
      item.refresh
    end
  end

  task :updateSpecial => :environment do
    @items = Item.all
    @items.each do |item|
      if item.price < item.higheast_price
        item.special = true
      else
        item.special = false
      end
      item.save
    end
  end

  task :updateBestSeller => :environment do
    items = [
      {"id"=>63080,"description"=>"Swisse 胶原蛋白液 500ml"},
      {"id"=>66552,"description"=>"Swisse 护肝片 120片"}, 
      {"id"=>67497,"description"=>"Swisse 蔓越莓 30粒"}, 
      {"id"=>67491,"description"=>"Swisse 钙+VD 150片"}, 
      {"id"=>72948,"description"=>"Swisse 卸妆水 300ml"}, 
      {"id"=>55300,"description"=>"Blackmores 鱼油(无腥) 400粒"}, 
      {"id"=>50409,"description"=>"Blackmores 孕妇黄金素 60粒"},
      {"id"=>44786,"description"=>"Blackmores 圣洁莓 40片"}, 
      {"id"=>53030,"description"=>"Blackmores 月见草油 190粒"}, 
      {"id"=>50366,"description"=>"Blackmores VC咀嚼片 200片"}, 
      {"id"=>65467,"description"=>"HealthyCare 葡萄籽 300粒"}, 
      {"id"=>64965,"description"=>"Nature's Way 儿童钙片 60片"}, 
      {"id"=>75447,"description"=>"BioIsland 儿童补锌 120片"},
      {"id"=>6740,"description"=>"Nulax 乐康膏 250g"}
    ]
    ids = []
    items.each{|item| ids<<item["id"]}
    @items = Item.all
    @items.each do |item|
      item.best_seller = ids.include?(item.identifier)
      item.save
    end
  end

  task :init => :environment do
    ids = [
    	{"id"=>67496,"description"=>"Swisse 胶原蛋白液 1L","brand"=>"Swisse"},
      {"id"=>63080,"description"=>"Swisse 胶原蛋白液 500ml","brand"=>"Swisse"},
      {"id"=>58348,"description"=>"Swisse 胶原蛋白 100片","brand"=>"Swisse"},
      {"id"=>67495,"description"=>"Swisse 胶原蛋白 180片","brand"=>"Swisse"},
      {"id"=>67493,"description"=>"Swisse 叶绿素(梅子味)口服液 500ml","brand"=>"Swisse"},
      {"id"=>58504,"description"=>"Swisse 叶绿素(薄荷味)口服液 500ml","brand"=>"Swisse"},
    	{"id"=>66552,"description"=>"Swisse 护肝片 120片","brand"=>"Swisse"}, 
    	{"id"=>67497,"description"=>"Swisse 蔓越莓 30粒","brand"=>"Swisse"}, 
    	{"id"=>67491,"description"=>"Swisse 钙+VD 150片","brand"=>"Swisse"}, 
    	{"id"=>73685,"description"=>"Swisse 葡萄籽精华 180片","brand"=>"Swisse"}, 
      {"id"=>66485,"description"=>"Swisse Q10-辅酶 50片","brand"=>"Swisse"},
    	{"id"=>72948,"description"=>"Swisse 卸妆水 300ml","brand"=>"Swisse"}, 
      {"id"=>56439,"description"=>"Swisse 前列康 50片","brand"=>"Swisse"},
      {"id"=>67970,"description"=>"Swisse 放松&助眠 60片","brand"=>"Swisse"},
      {"id"=>67490,"description"=>"Swisse 维生素B(缓解压力) 60片","brand"=>"Swisse"},
      {"id"=>55511,"description"=>"Swisse 男士综合维生素 120片","brand"=>"Swisse"},
      {"id"=>58346,"description"=>"Swisse 女士综合维生素 90片","brand"=>"Swisse"},
    	{"id"=>50010,"description"=>"Blackmores 鱼油 400粒","brand"=>"Blackmores"}, 
      {"id"=>55300,"description"=>"Blackmores 鱼油(无腥) 400粒","brand"=>"Blackmores"}, 
    	{"id"=>50409,"description"=>"Blackmores 孕妇黄金素 60粒","brand"=>"Blackmores"},
    	{"id"=>44786,"description"=>"Blackmores 圣洁莓 40片","brand"=>"Blackmores"}, 
    	{"id"=>53030,"description"=>"Blackmores 月见草油 190粒","brand"=>"Blackmores"}, 
    	{"id"=>58935,"description"=>"Blackmores 氨糖维骨力 90片","brand"=>"Blackmores"}, 
    	{"id"=>58934,"description"=>"Blackmores 氨糖维骨力 180片","brand"=>"Blackmores"}, 
      {"id"=>50366,"description"=>"Blackmores VC咀嚼片 200片","brand"=>"Blackmores"}, 
      {"id"=>39791,"description"=>"Blackmores 维生素B(缓解压力) 60片","brand"=>"Blackmores"},
    	{"id"=>65467,"description"=>"HealthyCare 葡萄籽 300粒","brand"=>"HealthyCare"}, 
      {"id"=>69367,"description"=>"HealthyCare 蜂胶(2000mg每粒) 200粒","brand"=>"HealthyCare"}, 
      {"id"=>56236,"description"=>"HealthyCare 蜂胶(1000mg每粒) 200粒","brand"=>"HealthyCare"}, 
      {"id"=>59407,"description"=>"HealthyCare 绵羊油+VE 100g","brand"=>"HealthyCare"}, 
      {"id"=>66544,"description"=>"HealthyCare 羊奶片(巧克力味) 300片","brand"=>"HealthyCare"},
      {"id"=>75460,"description"=>"HealthyCare 羊奶片(香草味) 300片","brand"=>"HealthyCare"},
      {"id"=>62704,"description"=>"HealthyCare 牛初乳粉 300g","brand"=>"HealthyCare"},
    	{"id"=>64965,"description"=>"Nature's Way 儿童钙片 60片","brand"=>"婴幼儿"}, 
      {"id"=>56442,"description"=>"Nature's Way 儿童Omega3鱼油(草莓味) 50粒","brand"=>"婴幼儿"}, 
      {"id"=>56442,"description"=>"Nature's Way 儿童混合维生素 60片","brand"=>"婴幼儿"}, 
      {"id"=>62713,"description"=>"Nature's Way 儿童维C 60片","brand"=>"婴幼儿"},
      {"id"=>63092,"description"=>"Aptamil 金装爱他美1段(0~6个月) 900g","brand"=>"婴幼儿"},
      {"id"=>63093,"description"=>"Aptamil 金装爱他美2段(6~12个月) 900g","brand"=>"婴幼儿"},
      {"id"=>63148,"description"=>"Aptamil 金装爱他美3段(1岁+) 900g","brand"=>"婴幼儿"},
      {"id"=>63150,"description"=>"Aptamil 金装爱他美4段(2岁+) 900g","brand"=>"婴幼儿"},
      {"id"=>73142,"description"=>"Aptamil 白金爱他美3段(1岁+) 900g","brand"=>"婴幼儿"},
      {"id"=>47951,"description"=>"Baby Balsam婴幼儿感冒舒缓膏 50g","brand"=>"婴幼儿"},
    	{"id"=>75447,"description"=>"BioIsland 儿童补锌 120片","brand"=>"婴幼儿"},
      {"id"=>77619,"description"=>"Bioglan 清烟净肺 60片","brand"=>"其它"},
      {"id"=>63835,"description"=>"Comvita 曼努卡蜂蜜(活性5+) 1kg","brand"=>"其它"},
      {"id"=>64119,"description"=>"Comvita 曼努卡蜂蜜(活性10+) 500g","brand"=>"其它"},
      {"id"=>64186,"description"=>"Comvita 曼努卡蜂蜜(活性15+) 250g","brand"=>"其它"},
      {"id"=>41902,"description"=>"ThursdayPlantation 茶树油祛痘凝胶 25g","brand"=>"其它"},
      {"id"=>6740,"description"=>"Nulax 乐康膏 250g","brand"=>"其它"},
      {"id"=>6741,"description"=>"Nulax 乐康膏 500g","brand"=>"其它"},
      {"id"=>41820,"description"=>"Lucas 木瓜膏 25g","brand"=>"其它"},
      {"id"=>31207,"description"=>"Lucas 木瓜膏 75g","brand"=>"其它"},
      {"id"=>7786,"description"=>"SM-33 口腔溃疡软膏 10g","brand"=>"其它"},
      {"id"=>64608,"description"=>"Restoria 发色还原乳 250ml","brand"=>"其它"},
      {"id"=>5875,"description"=>"脚气膏 15g","brand"=>"其它"},
      {"id"=>7267,"description"=>"痔疮膏 15g","brand"=>"其它"},
      {"id"=>7270,"description"=>"痔疮栓 12支","brand"=>"其它"},
      {"id"=>72907,"description"=>"女士爱乐维 100片","brand"=>"其它"}
    ]
    Item.delete_all
    Brand.delete_all
    ids.each do |id|
      @brand = Brand.find_or_create_by(name: id["brand"])
      @item = Item.find_or_create_by(identifier: id["id"])
      @item.description = id["description"]
      @item.brand = @brand
      @item.refresh()
    end
  end
end